import Vue from 'vue'
import App from './App'
import store from './store/index.js'

Vue.config.productionTip = false

App.mpType = 'app'
// 引入UI库
import uView from "uview-ui";
Vue.use(uView);

// 引入echarts
 import echarts from 'echarts'
 Vue.prototype.$echarts = echarts
 
const app = new Vue({
    ...App,
		store
})
app.$mount()
