module.exports = {
  // 选项...
	  devServer: {
	    proxy: {
	      '/api': {
	        target: 'http://localhost:3005',
	        changeOrigin: true,
					pathRewrite: {
						'^/api': '' // 重写路径
					}
	      },
	    }
	  }
}