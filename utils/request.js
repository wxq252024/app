import config from './config.js'
export default function(url, data, method) {
	return new Promise((resolve, reject) => {
		wx.request({
			url: config.host + url,
			data,
			method,
			success: res => {
				resolve(res.data)
			},
			fail: error => {
				reject(error)
			}
		})
	})
}
